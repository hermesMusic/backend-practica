package com.hermes.practica.Repository;

import com.hermes.practica.Entities.Usuario;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsuarioRepository extends CrudRepository<Usuario, Long>{
    
    Usuario findByNombre(String nombre);
    
}
