package com.hermes.practica.Controller;

import com.hermes.practica.Entities.Usuario;
import com.hermes.practica.Service.UsuarioService;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/Usuario")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;
    
    @PostMapping("/guardar")
    public void guardarUsuario(@RequestBody Usuario u){
        usuarioService.guardarUsuario(u);
    }
    
    @GetMapping("/obtener")
    public List<Usuario> obtenerUsuarios(){
        return (List<Usuario>) usuarioService.obtnerUsuarios();
    }
    
    @DeleteMapping("/eliminar/{id_usuario}")
    public void eliminarUsuario(@PathVariable Long id_usuario){
        usuarioService.eliminarUsuario(id_usuario);
    }
    
    @GetMapping("/obtenerUsuario/{id_usuario}")
    public Usuario obtenerUsuario(@PathVariable Long id_usuario){
        return usuarioService.obtenerUsuario(id_usuario);
    }
    
}
