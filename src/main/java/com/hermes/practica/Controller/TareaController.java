package com.hermes.practica.Controller;

import com.hermes.practica.Entities.Tarea;
import com.hermes.practica.Service.TareaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/Tarea")
public class TareaController {

    @Autowired
    private TareaService tareaService;

    @PostMapping("/guardar")
    public void guardarTarea(@RequestBody Tarea t){
        tareaService.guardarTarea(t);
    }

    @GetMapping("/obtener")
    public List<Tarea> obtenerTareas(){
        return (List<Tarea>) tareaService.obtnerTareas();
    }

    @DeleteMapping("/eliminar/{id_tarea}")
    public void eliminarTarea(@PathVariable Long id_tarea){
        tareaService.eliminarTarea(id_tarea);
    }

    @GetMapping("/obtenerTarea/{id_tarea}")
    public Tarea obtenerTarea(@PathVariable Long id_tarea){
        return tareaService.obtenerTarea(id_tarea);
    }

}
