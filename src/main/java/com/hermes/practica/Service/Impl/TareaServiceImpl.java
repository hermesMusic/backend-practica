package com.hermes.practica.Service.Impl;

import com.hermes.practica.Entities.Tarea;
import com.hermes.practica.Repository.TareaRepository;
import com.hermes.practica.Service.TareaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TareaServiceImpl implements TareaService {

    @Autowired
    private TareaRepository tareaRepository;


    @Override
    public void guardarTarea(Tarea t) {
        tareaRepository.save(t);

    }

    @Override
    public List<Tarea> obtnerTareas() {
        return  (List<Tarea>)tareaRepository.findAll();
    }

    @Override
    public Tarea obtenerTarea(Long id_tarea) {
        return tareaRepository.findById(id_tarea).get();
    }

    @Override
    public void eliminarTarea(Long id_tarea) {
        tareaRepository.deleteById(id_tarea);

    }
}
