package com.hermes.practica.Service.Impl;

import com.hermes.practica.Entities.Usuario;
import com.hermes.practica.Repository.UsuarioRepository;
import com.hermes.practica.Service.UsuarioService;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsuarioServiceImpl implements UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Override
    public void guardarUsuario(Usuario u) {
        usuarioRepository.save(u);
    }

    @Override
    public List<Usuario> obtnerUsuarios() {
        return (List<Usuario>) usuarioRepository.findAll();
    }

    @Override
    public void eliminarUsuario(Long id_usuario) {
        usuarioRepository.deleteById(id_usuario);
    }

    @Override
    public Usuario obtenerUsuario(Long id_usuario) {
        return usuarioRepository.findById(id_usuario).get();

    }

}
