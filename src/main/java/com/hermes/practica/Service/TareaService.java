package com.hermes.practica.Service;

import com.hermes.practica.Entities.Tarea;

import java.util.List;

public interface TareaService {
    public void guardarTarea(Tarea t);

    public List<Tarea> obtnerTareas();

    public Tarea obtenerTarea(Long id_tarea);

    public void eliminarTarea(Long id_tarea);
}
