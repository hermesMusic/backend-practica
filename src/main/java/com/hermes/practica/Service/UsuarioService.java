package com.hermes.practica.Service;

import com.hermes.practica.Entities.Usuario;

import java.util.List;

public interface UsuarioService {
    
    public void guardarUsuario(Usuario u);
    
    public List<Usuario> obtnerUsuarios();
    
    public Usuario obtenerUsuario(Long id_usuario);
    
    public void eliminarUsuario(Long id_usuario);
    
}
